# Positive Tombola ÐApp
### For training Ethereum & Web3.0 2018

# Prerequisites
### 1. Install Metamask
Supported browsers: Chrome, Firefox, Opera & Brave
https://metamask.io/

### 2. Install NodeJs
https://nodejs.org/en/

### 3. Set up dev environment
Please have a terminal (powershell, xterm, urxvt...) or an IDE (vscode, atom...).

Test if node is installed
```
$ node -v
v11.2.0
$ npm -v
6.4.1
```
Test if git is installed
```
$ git --version
git version 2.19.2
```
Clone our little ÐApp
```
$ cd your/preferred/destination/
$ git clone https://gitlab.com/goosecoid/ethereum-training.git
$ cd positive-tombola/
```
Install ganache
https://truffleframework.com/ganache
Check if it runs and check if ip is set to http://127.0.0.1 and port to 8545
Set mining cycle to 3 seconds

Install and run 
```
$ npm i -g truffle
$ npm i
$ truffle migrate
$ npm run
```

# Write your first smartcontract truffle

### Tombola business logic
