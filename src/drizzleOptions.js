import Tombola from './contracts/Tombola.json'

const drizzleOptions = {
    web3: {
        block: false,
        fallback: {
            type: 'ws',
            url: 'ws://127.0.0.1:8545'
        }
    },
    contracts: [
        Tombola,
    ],
}

export default drizzleOptions