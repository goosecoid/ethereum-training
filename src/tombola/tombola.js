import React, { Component } from 'react';
import { ContractData, AccountData, ContractForm } from 'drizzle-react-components';
import PropTypes from 'prop-types';



class Tombola extends Component {
    constructor(props, context) {
        super(props);
    }

    render() {
        return (
            <main className="container">
                <div className="pure-g">
                    <div className="pure-u-1-1 header">
                        <h1>Tombola Dapp</h1>
                    </div>

                    <div className="pure-u-1-1">
                        <h2>Winner</h2>
                        <ContractData contract="Tombola" method="winner" />
                    </div>

                    <div className="pure-u-1-1">
                        <h2>Wallet address</h2>
                        <AccountData accountIndex="0" units="ether" precision="3" />
                    </div>

                    <div className="pure-u-1-1">
                        <h2>Tombola data</h2>
                        <h3>Total tickets available for purchase:</h3>   
                        <ContractData contract="Tombola" method="finalTicketCount"/><br/>
                        <h3>Number of tickets already purchased:</h3>    
                        <ContractData contract="Tombola" method="purchasedTickets" /><br/>
                        <h3>Owner of the SC:</h3>
                        <ContractData contract="Tombola" method="owner" /><br/>
                    </div>

                    <div className="pure-u-1-1">
                        <h2>Register</h2>
                        <p>Please do so, it is required before buying</p>
                        <ContractForm contract="Tombola" method="registerParticipant" labels={['Username', 'Wallet Address']}/>
                    </div>

                    <div className="pure-u-1-1">
                        <h2>Purchase tickets</h2>
                        <ContractForm contract="Tombola" method="buyTicket" labels={['Wallet Address']}/>
                    </div>

                    <div className="pure-u-1-1">
                        <h2>Owner can draw winner</h2>
                        <ContractForm contract="Tombola" method="drawWinner"/>
                    </div>

                </div>
            </main>
        )
    }
}

Tombola.contextTypes = {
    drizzle: PropTypes.object
}

export default Tombola