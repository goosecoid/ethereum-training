import Tombola from './tombola'
import { drizzleConnect } from 'drizzle-react'

// May still need this even with data function to refresh component on updates for this contract.
const mapStateToProps = state => {
    return {
        accounts: state.accounts,
        Tombola: state.contracts.Tombola,
        drizzleStatus: state.drizzleStatus
    }
}

const TombolaContainer = drizzleConnect(Tombola, mapStateToProps);

export default TombolaContainer