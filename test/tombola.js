var Tombola = artifacts.require("Tombola");

contract('Tombola', async (accounts) => {
    it("Test greeting", async () => {
        let instance = await Tombola.deployed();
        let desiredValue = await instance.greeting();

        assert.equal(desiredValue.valueOf(), "Hello World!");
    });
})