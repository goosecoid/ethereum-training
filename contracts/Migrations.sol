pragma solidity ^0.4.24;


// contract required for the truffle migrate/deploy comand to work
// detects updated/new contracts declared in deployment files and only deploys changes)
contract Migrations {
    address public owner;
    uint public lastCompletedMigration;

    modifier restricted() {
        if (msg.sender == owner) _;
    }

    constructor() public {
        owner = msg.sender;
    }

    // completion of a migrate command triggers this function (1 execution per deployement file)
    function setCompleted(uint completed) public restricted {
        lastCompletedMigration = completed;
    }

    function upgrade(address newAddress) public restricted {
        Migrations upgraded = Migrations(newAddress);
        upgraded.setCompleted(lastCompletedMigration);
    }
}