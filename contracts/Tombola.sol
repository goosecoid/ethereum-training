pragma solidity ^0.4.24;


contract Tombola {
    struct Ticket {
        bool purchased;
        address owner;
        bool exists;
    }

    bool public winnerDrawed;
    uint public winningNumber;
    Participant public winner;

    struct Participant {
        bool isValue;
        address participantAddress;
        string name;
        bool hasTicket;
    }

    mapping (address => Participant) public participants;
    mapping (uint => Ticket) public totalOfTickets;
    uint public finalTicketCount;
    uint public currentTicketCount;
    uint public purchasedTickets;
    address public owner;

    constructor() public {
        owner = 0xeC7d095Fe8DF968Ede25C5b84287f27c69B8e3f9;
        finalTicketCount = 100; // define number of tickets that will be available for purchase
        currentTicketCount = 0;
        purchasedTickets = 0;
        createTickets(finalTicketCount);   
        winnerDrawed = false;  
    }

    function registerParticipant(string _name, address _participantAddress) public returns (bool) {
        require(!participants[_participantAddress].isValue);
        Participant memory _participant = Participant(true, _participantAddress, _name, false);
        participants[_participantAddress] = _participant;
        return true;
    }

    function buyTicket(address _participantAddress) public returns (bool) {
        require(finalTicketCount > purchasedTickets, "All tickets purchased");
        require(participants[_participantAddress].isValue, "Participant is not registered");   
        require(!participants[_participantAddress].hasTicket);
        require(!totalOfTickets[purchasedTickets].purchased);
        totalOfTickets[purchasedTickets].owner = _participantAddress;
        totalOfTickets[purchasedTickets].purchased = true;
        participants[_participantAddress].hasTicket = true;
        purchasedTickets++;
        return true;
    }

    function drawWinner() public {
        random();
        Ticket memory winningTicket = totalOfTickets[winningNumber];
        winner = participants[winningTicket.owner];
    }

    function random() internal {
        require(msg.sender == owner, "Only the owner can draw the winner");
        require(!winnerDrawed, "Winner is already drawn!");
        uint nonce;
        winningNumber = uint(keccak256(abi.encodePacked(now, msg.sender, nonce))) % (purchasedTickets);
        winnerDrawed = true;
    }

    function createTickets(uint _ticketCount) internal returns (string) {
        require(_ticketCount > 0);
        for (uint i  = 0; i < _ticketCount; i++) {
            Ticket memory _ticket  = Ticket(false, address(0), true);
            totalOfTickets[i] = _ticket;
            currentTicketCount++;
        } 
    }
}