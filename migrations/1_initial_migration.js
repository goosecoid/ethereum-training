var Migrations = artifacts.require("Migrations");
var Tombola = artifacts.require("Tombola");

module.exports = function(deployer) {
    deployer.deploy(Migrations);
    deployer.deploy(Tombola);
};